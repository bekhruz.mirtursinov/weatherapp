import React from "react";
import "./hourlyWeather.css";

function HourlyWeather(props) {
  const table = props.state.dates.map(date => {
    return (
      <tbody key={date} className="wh-table-body">
        <tr className="wh-date">
          <td className="wh-date">
            {props.state.months[date.split("-")[1]]} {date.split("-")[0]}
          </td>
          <td> {props.state.weekDays[date.split("-")[2]]}</td>
        </tr>
        {props.state.weatherdaily
          .filter(threeHourlyWeather => {
            var hourdate = new Date(threeHourlyWeather.dt_txt);
            return hourdate.getDate() == date.split("-")[0];
          })
          .map(specifiedWeather => {
            console.log(specifiedWeather);
            return (
              <tr
                key={new Date(specifiedWeather.dt_txt).getHours() + 1}
                className="wh-table"
              >
                <td className="wh-table-info">
                  <span className="wh-hour">
                    {("0" + new Date(specifiedWeather.dt_txt).getHours()).slice(
                      -2
                    )}
                    :
                    {(
                      "0" + new Date(specifiedWeather.dt_txt).getMinutes()
                    ).slice(-2)}
                  </span>
                  <img
                    className="wh-icon"
                    src={`http://openweathermap.org/img/wn/${specifiedWeather.weather[0].icon}.png`}
                  />
                </td>
                <td className="wh-description">
                  <p className="wh-degree">
                    {(specifiedWeather.main.temp - 273.15).toFixed(1) + "℃"}
                  </p>
                  <p className="wh-main">{specifiedWeather.weather[0].main}</p>
                  <p className="wh-wh-description">
                    {specifiedWeather.weather[0].description}
                  </p>
                  <p className="wh-speed">{specifiedWeather.wind.speed} m/s</p>
                </td>
              </tr>
            );
          })}
      </tbody>
    );
  });

  return (
    <div>
      <div className="weather-daily">
        <table className="wh-main-table">{table}</table>
      </div>
    </div>
  );
}

export default HourlyWeather;
