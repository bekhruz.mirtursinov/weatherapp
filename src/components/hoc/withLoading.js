import React from "react";
import "./withLoading.css";

function WithLoading(Component) {
  console.log("withloading");

  return function WihLoadingComponent({ isLoading, ...props }) {
    console.log("withloadingcomponent", props);
    if (!isLoading) {
      return <Component {...props} />;
    } else {
      return (
        <div className="loading-contianer">
          <div className="loader">
            <span>Loading...</span>
          </div>
          <p> Fetching data may take some time</p>
        </div>
      );
    }
  };
}

export default WithLoading;
