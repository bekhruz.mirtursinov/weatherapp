import React from "react";

import "./message.css";
const refreshPage = () => {
  console.log("refreshPage");
  window.location.reload(true);
};

const Message = props => {
  return (
    <div className="errorMessage">
      <div className="errorText">{props.message}</div>
      <button onClick={refreshPage}>Try Again</button>
    </div>
  );
};

export default Message;
