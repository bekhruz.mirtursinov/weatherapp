import React from "react";
import "./dailyWeather.css";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";

function DailyWeather(props) {
  console.log("city : ", props.city.city);
  const hightChart = props.city.city ? (
    <HighchartsReact
      className="highchart"
      highcharts={Highcharts}
      // constructorType={"stockChart"}
      options={{
        title: {
          text: "Daily Weather: " + props.city.city.name
        },
        series: [
          {
            name: "Temperature",
            data: props.data
          }
        ]
      }}
    />
  ) : null;
  // const { cityName } = props.city;
  return <div>{hightChart}</div>;
}

export default DailyWeather;
