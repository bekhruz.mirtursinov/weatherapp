import React, { Component } from "react";
import axios from "axios";
import { Route, Link, Switch, BrowserRouter as Router } from "react-router-dom";
import "./App.css";

import keys from "./components/utils";

import DailyWeather from "./components/dailyWeather/dailyWeather";
import HourlyWeather from "./components/hourlyWeather/hourlyWeather";
import WithLoading from "./components/hoc/withLoading";
import Message from "./components/message/message";

const DailyWeatherWithLoad = WithLoading(DailyWeather);
const HourlyWeatherWithLoad = WithLoading(HourlyWeather);

class App extends Component {
  state = {
    loading: false,
    weather: [],
    weatherdaily: [],
    dates: [],
    days: [],
    weekDays: [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      "Sunday"
    ],
    months: [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ],
    data: [],
    errorMessage: ""
  };

  creatingChart() {
    const chartData = this.state.weatherdaily.map(value => {
      console.log(value.dt_txt.split("-")[2].split(" ")[0]);
      return [value.dt_txt, parseInt(value.main.temp - 273.95)];
    });
    return chartData;
  }
  distinct(value, index, self) {
    return self.indexOf(value) === index;
  }

  async getdata() {
    try {
      this.setState({ loading: true });
      await axios
        .get(
          `https://api.openweathermap.org/data/2.5/forecast?q=Tashkent,uzb&appid=${keys.appid}`
        )
        .then(res => {
          const weather = res.data;
          this.setState({ weather });
          this.setState({ weatherdaily: weather.list });
          const dates = [];
          const days = [];

          weather.list.map(element => {
            const day = new Date(element.dt_txt);
            dates.push(
              day.getDate() + "-" + day.getMonth() + "-" + day.getDay()
            );
          });

          console.log(weather);

          this.setState({ dates: dates.filter(this.distinct) });
          this.setState({ weekdays: days.filter(this.distinct) });
          this.setState({ data: this.creatingChart() });
          this.setState({ loading: false });
        });
    } catch (error) {
      this.setState({ errorMessage: error.message });
      this.setState({ loading: false });
    }
  }

  componentDidMount() {
    this.getdata();
  }

  render() {
    const header =
      !this.state.loading && !this.state.errorMessage ? (
        <div className="header">
          <Link to="/">Daily</Link>
          <Link to="/hourly">Hourly</Link>
        </div>
      ) : null;

    console.log("Error: ", this.state.errorMessage);
    console.log("Error: ", !!this.state.errorMessage);
    const error = !!this.state.errorMessage ? (
      <Message message={this.state.errorMessage} />
    ) : null;

    return (
      <div>
        <div>{header}</div>
        <div>{error}</div>
        <Switch>
          <Route
            exact
            path="/"
            component={() => (
              <DailyWeatherWithLoad
                isLoading={this.state.loading}
                data={this.state.data}
                city={this.state.weather}
              />
            )}
          />
          <Route
            path="/hourly"
            component={() => (
              <HourlyWeatherWithLoad
                isLoading={this.state.loading}
                state={this.state}
              />
            )}
          />
        </Switch>
      </div>
    );
  }
}

export default App;
